const button = document.querySelector('button');
const text = document.querySelector('.info');

async function getRequest() {
    const result = await fetch('https://api.ipify.org/?format=json');
    const data = await result.json();
    console.log(data)
    return data;
}

async function getDestination (ip) {
    const result = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,region,city,district;lang=ru`);
    const data = await result.json();
    console.log(data);
    return data;
}

function renderAddress (data){
    let inner = `Вас отследили! Ваш адрес: ${data.continent}, ${data.country}, ${data.region}, город ${data.city}, район ${data.district}`
    text.innerText = inner;
    return text;
}

button.addEventListener('click',function (e) {
    e.preventDefault();
    getRequest()
        .then(resolve => getDestination(resolve.ip))
        .then(resolve => renderAddress(resolve))
})


